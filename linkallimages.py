#! /usr/bin/python
'''
Copyright (C) 2005 Aaron Spike, aaron@ekips.org
              2015 Vasco Tenner

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
'''

import sys
sys.path.append('/usr/share/inkscape/extensions')
import inkex, base64, os
import gettext
_ = gettext.gettext

class MyEffect(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        self.OptionParser.add_option("-s", "--selectedonly",
            action="store", type="inkbool", 
            dest="selectedonly", default=False,
            help="Link only selected images")

    def effect(self):
        #first create path for extracted images
        svg = self.document.getroot()
        docname = svg.get(inkex.addNS('docname',u'sodipodi'))
        if docname is None: docname = self.args[-1]

        self.path = os.path.splitext(docname)[0]+'_images'
        #inkex.errormsg(_("Document name %s") % self.args[-1])
        if not os.path.isdir(self.path):
            os.makedirs(self.path)
        # if slectedonly is enabled and there is a selection only embed selected
        # images. otherwise embed all images
        if (self.options.selectedonly):
            self.linkSelected(self.document, self.selected)
        else:
            self.linkAll(self.document)

    def linkSelected(self, document, selected):
        self.document=document #not that nice... oh well
        path = '//svg:image'
        for node in self.document.getroot().xpath(path, namespaces=inkex.NSS):
            #yust try to link every image even if it is linked already
            self.linkImage(node)

    def linkAll(self, document):
        self.document=document #not that nice... oh well
        path = '//svg:image'
        for node in self.document.getroot().xpath(path, namespaces=inkex.NSS):
            self.linkImage(node)

    def linkImage(self, node):
        mimesubext={
            'png' :'.png',
            'bmp' :'.bmp',
            'jpeg':'.jpg',
            'jpg' :'.jpg', #bogus mime
            'icon':'.ico',
            'gif' :'.gif'
        }
        
        # exbed the first embedded image
        xlink = node.get(inkex.addNS('href','xlink'))
        if (xlink[:4]=='data'):
            comma = xlink.find(',')
            if comma>0:
                #get extension
                fileext=''
                semicolon = xlink.find(';')
                if semicolon>0:
                    # find extension
                    for sub in mimesubext.keys():
                        if sub in xlink[5:semicolon].lower():
                            fileext = mimesubext[sub]
                            break 

                    #save
                    fname = node.get('id') + fileext
                    fname = os.path.join(self.path, fname)
                    data = base64.decodestring(xlink[comma:])
                    open(fname,'wb').write(data)
                    node.set(inkex.addNS('href','xlink'),fname)

if __name__ == '__main__':
    e = MyEffect()
    e.affect()


# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 encoding=utf-8 textwidth=99
